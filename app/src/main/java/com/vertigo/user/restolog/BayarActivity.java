package com.vertigo.user.restolog;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class BayarActivity extends AppCompatActivity {

    EditText nama_pembeli;
    CheckBox Bakso, Nasi_Goreng, Mie_Goreng;
    Button nambah, ngurang, bayar;
    TextView jumlah, ringkasan;

    int quantity;
    int harga = 10;
    int total;
    String Summary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bayar);

        nama_pembeli = findViewById(R.id.editnama);
        Bakso = findViewById(R.id.chk_1);
        Nasi_Goreng = findViewById(R.id.chk_2);
        Mie_Goreng = findViewById(R.id.chk_3);
        nambah = findViewById(R.id.btn_plus);
        ngurang = findViewById(R.id.btn_minus);
        bayar = findViewById(R.id.btn_order);
        jumlah = findViewById(R.id.quantity);
        ringkasan = findViewById(R.id.summary);

        nambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                quantity = quantity + 1;
                jumlah.setText(String.valueOf(quantity));

            }
        });

        ngurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                quantity = quantity - 1;
                jumlah.setText(String.valueOf(quantity));

            }
        });

        bayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                total = harga * quantity;

                Summary = "Add Bakso = " + Bakso.isChecked() + "\n" +
                            "Add Nasi Goreng = " + Nasi_Goreng.isChecked() + "\n" +
                            "Add Mie Goreng = " + Mie_Goreng.isChecked() + "\n" +
                            "Price = " + total + "\n" +
                            "Atas Nama = " + nama_pembeli.getText().toString();

                ringkasan.setText(Summary);

            }
        });



    }
}

package com.vertigo.user.restolog;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    EditText namaid, katasandi;
    Button tombol_masuk;

    String user, password;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        namaid = findViewById(R.id.user);
        katasandi = findViewById(R.id.password);
        tombol_masuk = findViewById(R.id.login);

        intent = new Intent(this,MainActivity.class);

        user = namaid.getText().toString();
        password = katasandi.getText().toString();

        tombol_masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (namaid.getText().toString().equals("admin") && katasandi.getText().toString().equals("admin")) {

                    startActivity(intent);

                } else {

                    Toast.makeText(LoginActivity.this, "username dan password nya = admin", Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
}
